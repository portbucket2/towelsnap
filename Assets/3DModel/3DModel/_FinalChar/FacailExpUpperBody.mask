%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: FacailExpUpperBody
  m_Mask: 00000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bind_Hips
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot/Bind_LeftToeBase
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot/Bind_LeftToeBase/Bind_LeftToe_End
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot/Bind_RightToeBase
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot/Bind_RightToeBase/Bind_RightToe_End
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1/Bind_LeftHandIndex2
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1/Bind_LeftHandIndex2/Bind_LeftHandIndex3
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand/Bind_LeftHandIndex1/Bind_LeftHandIndex2/Bind_LeftHandIndex3/Bind_LeftHandIndex4
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Bind_HeadTop_End
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_Brow_1_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_Brow_1_L_Jnt/Eye_Brow_2_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_Brow_1_L_Jnt/Eye_Brow_2_L_Jnt/Eye_Brow_3_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_Brow_1_R_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_Brow_1_R_Jnt/Eye_Brow_2_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_Brow_1_R_Jnt/Eye_Brow_2_R/Eye_Brow_3_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Center_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Down_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Lash_Down_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Lash_Up_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Left_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Right_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_L_Jnt/Eye_Up_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Center_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Down_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Rash_Down_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Rash_Up_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Reft_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Right_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Eye_R_Jnt/Eye_Up_R
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_Down_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_Lower_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_Lower_R_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_R_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_Up_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_Upper_L_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Lip_Upper_R_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Tongue_1_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Tongue_1_Jnt/Tongue_2_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Tongue_1_Jnt/Tongue_2_Jnt/Tongue_3_Jnt
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1/Bind_RightHandIndex2
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1/Bind_RightHandIndex2/Bind_RightHandIndex3
    m_Weight: 0
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand/Bind_RightHandIndex1/Bind_RightHandIndex2/Bind_RightHandIndex3/Bind_RightHandIndex4
    m_Weight: 0
  - m_Path: Mesh_GRP
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Fatty
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Fatty/Mesh_Fatty_Body
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Fatty/Mesh_Fatty_Eye
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Fatty/Mesh_Fatty_Hair
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Fatty/Mesh_Fatty_Lips
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Fatty/Mesh_Fatty_Tongue
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Normal
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Normal/Mesh_Normal_Body
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Normal/Mesh_Normal_Eye
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Normal/Mesh_Normal_Hair
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Normal/Mesh_Normal_Lips
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Normal/Mesh_Normal_Tongue
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Spike
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Spike/Mesh_Spike_Body
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Spike/Mesh_Spike_Eye
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Spike/Mesh_Spike_Hair
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Spike/Mesh_Spike_Lips
    m_Weight: 0
  - m_Path: Mesh_GRP/Char_Spike/Mesh_Spike_Tongue
    m_Weight: 0
