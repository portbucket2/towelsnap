﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimPointBehaviour : MonoBehaviour
{
    public Animator anim;
    public bool onTarget;

   // public Camera mainCamera;
    public LayerMask layerMask;

    [Space(20)]
    public float force = 10f;
    public float forceOffset = 0.1f;
    public float forceRadius;
    public float forcePower;

    public static AimPointBehaviour Instance;
    internal Dictionary<string,Dictionary<Collider, MeshDeformer>> meshDeformerColliderDictionary = new Dictionary<string,Dictionary<Collider, MeshDeformer>>();
    

    public bool isTholthole;

    public bool isAttackStart;

    public ParticleSystem hitParticle;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        onTarget = false;
       // isTholthole = false;

        RaycastHit hitInfo;

       // Debug.DrawRay(transform.position, transform.forward, Color.yellow);
        if (isAttackStart)
        {
            // selectRayTrans = rayTransList[Random.Range(0, rayTransList.Count)];

            //(Physics.SphereCast(selectRayTrans.position, 0.4f, selectRayTrans.forward, out hitInfo, 100, layerMask, QueryTriggerInteraction.UseGlobal))
            
            bool lol = Physics.SphereCast(transform.position, 0.4f, transform.forward, out hitInfo, 100, layerMask, QueryTriggerInteraction.UseGlobal);
           
            if (lol)
            {
                var rig = hitInfo.collider;

               
                if (rig.CompareTag("NPC"))
                {

                    float t_currentHitDistance = Vector3.Distance(rig.transform.position, transform.position);
                   // Debug.LogFormat("{0}, {1}", hitInfo.collider.name, Time.time);
                    onTarget = true;

                    if (!isTholthole)
                    {
                        isTholthole = true;
                        MeshDeformer deformer = null; // hitInfo.collider.GetComponent<MeshDeformer>();

//                        Debug.Log(hitInfo.transform.root.name);


                        if (meshDeformerColliderDictionary.ContainsKey(hitInfo.transform.root.GetComponentInChildren<MeshDeformer>().id.ToString()))
                        {
                            Dictionary<Collider, MeshDeformer> dic = meshDeformerColliderDictionary[hitInfo.transform.root.GetComponentInChildren<MeshDeformer>().id.ToString()];

                            if (dic.ContainsKey(hitInfo.collider))
                            {
                                deformer = dic[hitInfo.collider];
                                
                            }
                        }
                        if (deformer)
                        {  
                            Vector3 point = hitInfo.point;
                            Vector3 modifiedPoint = new Vector3(point.x, Random.Range(1f, 2.5f), point.z);

                            Debug.Log("HERE ---- " + modifiedPoint);

                            hitParticle.transform.localPosition = -modifiedPoint;
                            hitParticle.Play();

                            modifiedPoint += hitInfo.normal * 0.1f;
                            Vector3 hitDirection = Vector3.Slerp(-hitInfo.normal, transform.forward, 0.5f).normalized;
                            deformer.AddDeformingForce(modifiedPoint, hitDirection, force, forceRadius);
                            hitInfo.transform.root.GetComponent<NPCMove>().ActivateCleanMAT();
                            hitInfo.transform.root.GetComponent<NPCMove>().isHit = true;

                            NPCMoveManager.Instance.PlayCheers();
                            NPCMoveManager.Instance.PlayHit();
                        }
                    }

                }
            }
        }

        anim.SetBool("onTarget", onTarget);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Debug.DrawRay(transform.position, transform.forward, Color.red);
        Gizmos.DrawWireSphere(transform.position + transform.forward, 0.1f);

    }
}
