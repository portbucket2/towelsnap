using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TowelCon : MonoBehaviour
{
    public static TowelCon Instance;

    [Range(0f, 1f)] public float delayTimer;
    [Range(0f, 1f)] public float holdTimer;
    public bool isHolding;
    public bool isResetSpeed;

    [Space(20)]
    [Header("Player Animation")]
    public Animator towelAnimator;

    private readonly int LEVEL_START = Animator.StringToHash("levelstart");
    private readonly int IDLE_GAME = Animator.StringToHash("idlegame");
    private readonly int ATTACK = Animator.StringToHash("attack");

    public bool isStop;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
    }

    private void Start()
    {
        
    }

    public void PlayLevelStart()
    {
        towelAnimator.SetTrigger(LEVEL_START);
    }
    public void PlayIdleGame()
    {
        towelAnimator.SetTrigger(IDLE_GAME);
    }
    public void PlayAttack()
    {
        towelAnimator.SetTrigger(ATTACK);
    }


    public void HoldingTrue()
    {
        isHolding = true;
    }

    public void ReleaseInput()
    {
        StartCoroutine(ReleaseRoutine());
    }

    IEnumerator ReleaseRoutine()
    {
        yield return new WaitForEndOfFrame();
        isHolding = false;
        holdTimer = 0;

        isPowerBarStart = false;

        if (isReady)
        {
            UIManager.Instance.PowerupReset();
          //  NPCMoveManager.Instance.SpeedReset();
            isResetSpeed = false;
            PlayAttack();

            
            NPCMoveManager.Instance.SpeedStop();
            
            isStop = true;
            yield return new WaitForSeconds(1.5f);
            NPCMoveManager.Instance.SpeedReset();
            isStop = false;
            isReady = false;
            
        }
        time = 0f;

    }

    float time = 0f;
    bool isReady;

    public void HoldInput()
    {
        time += Time.deltaTime;

        if (time > 0.2f)
        {
            isReady = true;

            StartCoroutine(HoldRoutine());

            if (!isResetSpeed)
            {
                //AimPointBehaviour.Instance.isAttackStart = true;
                isResetSpeed = true;
                NPCMoveManager.Instance.DecreasedSpeed();
            }
        }
    }

    public bool isCircle;

    public bool isPowerBarStart;

    private IEnumerator HoldRoutine()
    {
        while (isHolding)
        {
            delayTimer += Time.deltaTime;
            holdTimer += Time.deltaTime;
           
            if (delayTimer >0.3f)
            {
                delayTimer = 0;

                if (isCircle)
                {
                    if (!isPowerBarStart)
                    {
                        isPowerBarStart = true;
                        UIManager.Instance.PowerUpFiller(true);
                    }
                }
                else
                {
                    if (!isPowerBarStart)
                    {
                        isPowerBarStart = true;
                        UIManager.Instance.PowerUpFiller(false);
                    }
                }
               
                if(holdTimer > 2f)
                {
                    Debug.LogWarning("Power FUll");
                }
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void ActivateHand()
    {
        towelAnimator.gameObject.SetActive(true);
    }
    public void DeactivateHand()
    {
        towelAnimator.gameObject.SetActive(false);
    }
}
