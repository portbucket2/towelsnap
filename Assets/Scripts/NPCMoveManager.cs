using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class NPCMoveManager : MonoBehaviour
{
    public static NPCMoveManager Instance;
    public PathCreator pathCreator;

    public List<NPCMove> npcList;

    public bool isCreateNPC;

    

    public int idCounter = 0;


    public int destroyedChar;

    public int levelCharLimit;
    public int dirtyCharNumber;
    public int dirtyCounter;


    public List<NPCMove> currentNPCList;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
      
        
    }

    public void ActivateNPC()
    {
        StartCoroutine(NPCActivateRoutine());
    }

    IEnumerator NPCActivateRoutine()
    {
        while (isCreateNPC)
        {
            if (!TowelCon.Instance.isHolding && !TowelCon.Instance.isStop)
            {
                int randVal = UnityEngine.Random.Range(0, npcList.Count);
                GameObject go = Instantiate(npcList[randVal].gameObject);
                go.GetComponent<NPCMove>().pathMove = pathCreator;
                go.GetComponent<NPCMove>().Activate();
                go.GetComponentInChildren<MeshDeformer>().id = idCounter;  
                idCounter++;
                currentNPCList.Add(go.GetComponent<NPCMove>());

                if (dirtyCounter < dirtyCharNumber)
                {
                    int randValue = UnityEngine.Random.Range(0, 100);
                    if (randValue < 80)
                    {
                        go.GetComponent<NPCMove>().isDirty = true;
                        go.GetComponent<NPCMove>().ActivateDirtMAT();
                        dirtyCounter++;
                    }
                }
                float randWait = UnityEngine.Random.Range(3.75f, 4.5f);

                yield return new WaitForSeconds(randWait);

                if(idCounter == levelCharLimit)
                {
                    Debug.LogWarning("Char Reached on LIMIT");
                    break;
                }

            }
            yield return new WaitForEndOfFrame();
        }

        //for (int i = 0; i < npcList.Count; i++)
        //{
        //    npcList[i].Activate();
           
        //    yield return new WaitForSeconds(2.4f);
        //}
    }

    public void RemoveNPC(NPCMove nPCMove)
    {
        destroyedChar++;
        currentNPCList.Remove(nPCMove);

        if(destroyedChar == levelCharLimit)
        {
            Debug.LogWarning("LEVEL COMPleted, Decide Result and show SUCCESS / FAILED");
            Gameplay.Instance.LevelEnd();
        }
    }

    public void SpeedStop()
    {
        for (int i = 0; i < currentNPCList.Count; i++)
        {
            currentNPCList[i].SpeedStop();
        }
    }

    public void PlayHit()
    {
        for (int i = 0; i < currentNPCList.Count; i++)
        {
            currentNPCList[i].SelectRandomHitExp();
        }
    }

    public void PlayCheers()
    {
        for (int i = 0; i < currentNPCList.Count; i++)
        {
            currentNPCList[i].SelectRandomDance();
        }
    }

    public void SpeedReset()
    {
//        Debug.Log(currentNPCList.Count);

        for (int i = 0; i < currentNPCList.Count; i++)
        {
            currentNPCList[i].SpeedReset();
            currentNPCList[i].SpeedAnimationReset();
        }
    }

    public void DecreasedSpeed()
    {
        for (int i = 0; i < currentNPCList.Count; i++)
        {
            currentNPCList[i].SpeedDecresed();
            currentNPCList[i].SpeedAnimationDecreased();
        }
    }

    public int dirtyPassChar;

    public void DirtyCalculation(NPCMove nPCMove)
    {
        float val = (1f / dirtyCharNumber);
        if (nPCMove.isDirty)
        {
           // Debug.LogError("Call here " +val);
            UIManager.Instance.DirtyFill(val);
            dirtyPassChar++;
        }
    }

    public bool IsLevelComplete()
    {
        if(dirtyCharNumber == dirtyPassChar)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
