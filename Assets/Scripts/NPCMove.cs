using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class NPCMove : MonoBehaviour
{
    public Animator animator;

    public List<GameObject> movePointList;

    [Space(10)]
    public PathCreator pathMove;

    [Range(0f, 5f)] public float speed;
    public float distTravelled;
    public bool isStartMoving;


    public Material dirtyMAT;
    public Material cleanMAT;
    public SkinnedMeshRenderer skinnedMeshRenderer;

    public bool isHit;

    public bool isDirty;

    private void Awake()
    {
        animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
    }

    private void Update()
    {
        if (isStartMoving)
        {
            distTravelled += speed * Time.deltaTime;
            transform.position = pathMove.path.GetPointAtDistance(distTravelled);
            transform.rotation = pathMove.path.GetRotationAtDistance(distTravelled);
        }

        if((pathMove.path.length - distTravelled)<0.1f)
        {
            NPCMoveManager.Instance.RemoveNPC(this);

            Destroy(this.gameObject);
        }
    }

    public void ActivateAnimator()
    {
        animator.enabled = true;
        animator.enabled = false;
        animator.enabled = true;
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        isStartMoving = true;

        speed = UnityEngine.Random.Range(0.55f, .85f);

    }

    public void SpeedAnimationReset()
    {
        animator.SetFloat("animspeed", 1);
    }
    public void SpeedAnimationDecreased()
    {
        animator.SetFloat("animspeed", 0.3f);
    }

    public void SpeedStop()
    {
        isStartMoving = false;
    }

    public void SpeedReset()
    {
        StartCoroutine(SpeedResetRoutine());  
    }
    IEnumerator SpeedResetRoutine()
    {
        StopCoroutine(SpeedDecreasedRoutine());

        //Debug.Log(transform.name);
        yield return new WaitForEndOfFrame();
        speed = UnityEngine.Random.Range(0.5f, 0.95f);
        isStartMoving = true;
        PlayWalk();
//        Debug.Log("CALLING THER     " + speed);
    }

    private void PlayWalk()
    {
        animator.SetTrigger("walk");
    }

    public void SpeedDecresed()
    {
        StartCoroutine(SpeedDecreasedRoutine());
    }


    IEnumerator SpeedDecreasedRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.25f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = speed;
        float t_DestValue = speed - 0.3f;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            speed = Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(SpeedDecreasedRoutine());
    }

    public void ActivateCleanMAT()
    {
        Debug.Log("CALLING THIS --------------");
        skinnedMeshRenderer.material = cleanMAT;
        if (isDirty)
        {
            isDirty = false;
        }
    }
    public void ActivateDirtMAT()
    {
        skinnedMeshRenderer.material = dirtyMAT;
    }

    
    public void SelectRandomDance()
    {
        if (!isHit)
        {
            int t_RandValue = UnityEngine.Random.Range(0, 3);
            animator.SetTrigger("cheer_" + t_RandValue);
        }
    }
    public void SelectRandomHitExp()
    {
        if (isHit)
        {
            int t_RandValue = UnityEngine.Random.Range(0, 2);
            int tval = UnityEngine.Random.Range(0, 6);
            animator.SetTrigger("hitpose" + t_RandValue);
            animator.SetTrigger("hitex" + tval);
        }
    }


}
