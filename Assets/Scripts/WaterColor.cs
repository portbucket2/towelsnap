using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterColor : MonoBehaviour
{
    public static WaterColor Instance;

    public MeshRenderer waterMesh;
    public Material normalMAT;
    public Material dirtyMAT;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void NormalColor()
    {
        waterMesh.material = normalMAT;
    }
    public void DirtyColor()
    {
        waterMesh.material = dirtyMAT;
    }
}
