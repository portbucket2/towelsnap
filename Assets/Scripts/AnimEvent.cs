using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEvent : MonoBehaviour
{
    public void AttackStart()
    {
        AimPointBehaviour.Instance.isAttackStart = true;
    }
    public void AttackEnd()
    {
        AimPointBehaviour.Instance.isAttackStart = false;
        AimPointBehaviour.Instance.isTholthole = false;
    }
}
