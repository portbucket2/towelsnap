using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    public NPCMoveManager npcMoveManager;

    public TowelCon towelCon;

    public InputController inputController;

    [Space(20)]
    public Animator camTransAnim;
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");

    private string TUT_SHOW = "Tut_Show";

    public GameObject levelEndHolder;

    public GameObject levelFailedHolder;

    public bool isStart;


    private void Awake()
    {
        Application.targetFrameRate = 60;

        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        EnvLoader.Instance.ActivateEnv();

        GotoNextLevel();

        StartGame();
    }


    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    private IEnumerator StartGameRoutine()
    {
        yield return new WaitForEndOfFrame();

        AnalyticsAssitantGM.ReportLevelStart(LevelManager.Instance.gameCurrentLevel);

        WaterColor.Instance.NormalColor();
        yield return new WaitForSeconds(1f);
        UIManager.Instance.StartGame();
        yield return new WaitForSeconds(0.5f);

        int tut = PlayerPrefs.GetInt(TUT_SHOW, 0);
        if (tut == 0)
        {
            PlayerPrefs.SetInt(TUT_SHOW, 1);
            UIManager.Instance.ActivateTutorial();
            isStart = false;
        }
        towelCon.PlayLevelStart();
        yield return new WaitForSeconds(0.1f);
        camTransAnim.SetTrigger(ENTRY);

        while (!isStart)
        {
            yield return new WaitForEndOfFrame();
        }
        npcMoveManager.ActivateNPC();
        inputController.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        UIManager.Instance.ActivateDirtyBar();
    }

    public void LevelEnd()
    {
        StartCoroutine(LevelEndRoutine());
    }
    IEnumerator LevelEndRoutine()
    {
        yield return new WaitForSeconds(0.1f);
        inputController.gameObject.SetActive(false);
        camTransAnim.SetTrigger(EXIT);

        TowelCon.Instance.DeactivateHand();

        if (NPCMoveManager.Instance.IsLevelComplete())
        {
            AnalyticsAssitantGM.ReportLevelCompleted(LevelManager.Instance.gameCurrentLevel);
            levelEndHolder.SetActive(true);
            WaterColor.Instance.NormalColor();
            yield return new WaitForEndOfFrame();

            LevelManager.Instance.IncreaseGameLevel();
            yield return new WaitForSeconds(1f);
            UIManager.Instance.ShowLevelComplete();

        }
        else
        {
            AnalyticsAssitantGM.ReportMatchFailed(LevelManager.Instance.gameCurrentLevel);
            levelFailedHolder.SetActive(true);
            WaterColor.Instance.DirtyColor();
            yield return new WaitForSeconds(1f);
            UIManager.Instance.ActivateLevelFailed();
        }
    }

    public void GotoNextLevel()
    {
        if (LevelManager.Instance.GetCurrentLevel() > 5)
        {
            PlayerPrefs.DeleteKey("Game_Level_Pref");
        }

        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

//        Debug.Log(t_CurrentLevel + " Scene");
        string loadScene = "Level" + t_CurrentLevel;

        if (SceneManager.GetActiveScene().name != loadScene)
        {
            SceneManager.LoadScene("Level" + t_CurrentLevel);
        }

        //switch (t_CurrentLevel)
        //{
        //    case 1:
        //        SceneManager.LoadScene("Level" + LevelManager.Instance.GetCurrentLevel());
        //        break;
        //    case 2:
        //        SceneManager.LoadScene("Level" + LevelManager.Instance.GetCurrentLevel());
        //        break;


        //    default:
        //        break;
        //}
    }

    public void LoadAgain()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        Debug.Log(t_CurrentLevel + " Scene");
        string loadScene = "Level" + t_CurrentLevel;

        
        SceneManager.LoadScene("Level" + t_CurrentLevel);
        
    }
}
