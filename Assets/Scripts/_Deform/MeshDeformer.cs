﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//[RequireComponent(typeof(MeshFilter))]
public class MeshDeformer : MonoBehaviour
{

    public int id;
    public List<Collider> myColliders;
    public const float AmpDamping_HalfLife = .1f;
    public const float frequency = 2.5f;

    public int deformerCount = 0;

    Mesh deformingMesh;
	Vector3[] originalVertices, displacedVertices;

    Vector3[] deformDirection;
    float[] deformAmplitude;

    float deformStartTime;
	SkinnedMeshRenderer skinnedMesh;

	void Start ()
	{
        if(myColliders.Count == 0)
        {
            Collider[] c = this.transform.root.GetComponentsInChildren<Collider>();
            if (c.Length>0) myColliders.AddRange(c);
        }
        Dictionary<Collider, MeshDeformer> tempDic = new Dictionary<Collider, MeshDeformer>();
        foreach (var item in myColliders)
        {
            tempDic.Add(item, this);
           
        }
        AimPointBehaviour.Instance.meshDeformerColliderDictionary.Add(id.ToString(),tempDic);
//        Debug.LogWarning("ADDEDDD id " +id);

        skinnedMesh = GetComponent<SkinnedMeshRenderer>();
		
		deformingMesh =Instantiate(skinnedMesh.sharedMesh);
		skinnedMesh.sharedMesh = deformingMesh;

		
		
		originalVertices = deformingMesh.vertices;
		displacedVertices = new Vector3[originalVertices.Length];
		for (int i = 0; i < originalVertices.Length; i++) {
			displacedVertices[i] = originalVertices[i];
		}
        deformDirection = new Vector3[originalVertices.Length];
        deformAmplitude = new float[originalVertices.Length];
    }

	void Update()
	{
        if (deformerCount > 0)
        {
            UpdateVertex();


        }
    }


    //Vector3 vVel;
    Vector3 dist_vec;

    Vector3 v_disp;
    Vector3 v_orgn;
    float multiplierV;
    float deltaTime;
    float  attenuatedAmp;
    Vector3 deformDir;//, displacementValue;
    void UpdateVertex ()
    {
        float elaspedTime = (Time.time - deformStartTime);
        float dampRatio = Mathf.Pow(0.5f, elaspedTime / AmpDamping_HalfLife);
        deltaTime = Time.deltaTime;
        float y = Mathf.Sin(2 * Mathf.PI * frequency * elaspedTime);// Mathf.Pow( elaspedTime, FreqDampPower));
        for (int i = 0; i < displacedVertices.Length; i++)
        {
            v_orgn = originalVertices[i];
            deformDir = deformDirection[i];
            attenuatedAmp = dampRatio * deformAmplitude[i];
            //displacementValue = attenuatedAmp * y * deformDir;
            v_disp.x = v_orgn.x + attenuatedAmp * y * deformDir.x;
            v_disp.y = v_orgn.y + attenuatedAmp * y * deformDir.y;
            v_disp.z = v_orgn.z + attenuatedAmp * y * deformDir.z;

            displacedVertices[i] = v_disp;
        }
        deformingMesh.vertices = displacedVertices;
        deformingMesh.RecalculateNormals();



	}

    float mag_VtoP, r, attenuatedForce,sine,vel;
    Vector3 VtoP,VtoP_norm;
	public void AddDeformingForce (Vector3 point, Vector3 normal, float waveAmplitude, float impactRadius)
    {
        point = transform.InverseTransformPoint(point);
        impactRadius = transform.InverseTransformVector(impactRadius * Vector3.up).magnitude*1.00f;
        waveAmplitude = transform.InverseTransformVector(waveAmplitude * Vector3.up).magnitude/450.0f;
        deformStartTime = Time.time;
        deltaTime = Time.deltaTime;
        deformerCount++;
        for (int i = 0; i < originalVertices.Length; i++) {
            v_orgn = originalVertices[i];
            VtoP.x = v_orgn.x - point.x;
            VtoP.y = v_orgn.y - point.y;
            VtoP.z = v_orgn.z - point.z;
            mag_VtoP = Mathf.Sqrt(VtoP.x * VtoP.x + VtoP.y * VtoP.y + VtoP.z * VtoP.z);

            if (mag_VtoP <= impactRadius)
            {
                VtoP_norm.x = VtoP.x / mag_VtoP;
                VtoP_norm.y = VtoP.y / mag_VtoP;
                VtoP_norm.z = VtoP.z / mag_VtoP;
                r = 1 - (mag_VtoP / impactRadius);
                //sine = Mathf.Sin(r * (Mathf.PI / 2));
                sine = (Mathf.Sin((r - 0.5f) * Mathf.PI) + 1)*0.5f;
                attenuatedForce = sine * waveAmplitude;
                //vel = attenuatedForce * deltaTime;

                deformDirection[i] = -normal;
                deformAmplitude[i] = attenuatedForce;
            }
            else
            {
                deformAmplitude[i] = 0;
            }



		}

	}


	private IEnumerator coroutine;

    public void DeactivateVertexMotion()
    {
		coroutine = Wait();

		//StopCoroutine(coroutine);

		StartCoroutine(coroutine);
    }
	IEnumerator Wait()
	{
		yield return new WaitForSeconds(1.25f);
        deformerCount--;
        if (deformerCount == 0)
        {
            for (int i = 0; i < originalVertices.Length; i++)
            {
                displacedVertices[i] = originalVertices[i];

            }
            deformingMesh.vertices = displacedVertices;
            deformingMesh.RecalculateNormals();
        }
        //verCounter = 0;
        //isVertexMotion = false;
    }
}