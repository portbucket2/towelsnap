﻿using UnityEngine;

public class MeshDeformerInput : MonoBehaviour {
	
	public float force = 10f;
	public float forceOffset = 0.1f;
	public float forceRadius;
	public float forcePower;

	public Camera mainCam;
	
	void Update () {
		if (Input.GetMouseButton(0)) {
			HandleInput();
		}
	}

	void HandleInput () {
//		Debug.Log("HERE out");
		Ray inputRay = mainCam.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(inputRay, out hit))
		{

//			Debug.Log(hit.collider.name);
			MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
			if (deformer)
			{
				//Debug.Log("HERE");
				Vector3 point = hit.point;
				point += hit.normal * forceOffset;
				//deformer.AddDeformingForce(point, force,forceRadius,forcePower);
			}
		}
	}
}