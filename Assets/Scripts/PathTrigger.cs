using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("NPC"))
        {
            
            NPCMoveManager.Instance.DirtyCalculation(other.transform.root.GetComponent<NPCMove>());
        }
    }
}
