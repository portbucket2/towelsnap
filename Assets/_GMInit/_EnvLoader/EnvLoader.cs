﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvLoader : MonoBehaviour
{
    public static EnvLoader Instance;

    public GameObject parkRef;

    public GameObject parkEnv;

    private void Awake()
    {
        if(Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
            parkEnv = Instantiate(parkRef);
            parkEnv.transform.SetParent(this.transform);
            
        }
        else
        {
            
            DestroyImmediate(this.gameObject);
        }

        Instance.parkEnv.SetActive(false);
    }

    public void ActivateEnv()
    {
        parkEnv.SetActive(true);
    }
    public void DeactivateEnv()
    {
        parkEnv.SetActive(false);
    }
}
