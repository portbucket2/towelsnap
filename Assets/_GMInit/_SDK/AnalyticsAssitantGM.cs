﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using GameAnalyticsSDK;


public static class AnalyticsAssitantGM
{
    public static void ReportLevelStart(int levelNumber)
    {
        GAnalyticsGM.Instance.LevelStarted(levelNumber);
        Debug.Log("LEvel StartTED " +levelNumber);
    }
    public static void ReportLevelCompleted(int levelNumber)
    {
        GAnalyticsGM.Instance.LevelCompleted(levelNumber);
        Debug.Log("LEvel COMPLETED " + levelNumber);
    }
   
    public static void ReportMatchFailed(int levelNumber)
    {
        GAnalyticsGM.Instance.LevelFailed(levelNumber);
        Debug.Log("LEvel FAILED " + levelNumber);
    }
}
