﻿
    using System.Collections.Generic;
    using UnityEngine;
    using Facebook.Unity;

    public class FacebookAnalyticsManager : MonoBehaviour
    {

        public static FacebookAnalyticsManager Instance;

        void Awake()
        {

            if (Instance == null)
            {

                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #region Public Callback

        public void FBALevelStart(int t_CurrentLevel)
        {
            var t_FBEventParameter = new Dictionary<string,object>();
            t_FBEventParameter["Level_Info"] = "" + t_CurrentLevel;
            FB.LogAppEvent(
                "Level_Start",
                null,
                t_FBEventParameter
            );

        }

        public void FBALevelComplete(int t_CurrentLevel)
        {
            var t_FBEventParameter = new Dictionary<string,object>();
            t_FBEventParameter["Level_Info"] = "" + t_CurrentLevel;
            FB.LogAppEvent(
                "Level_Achieved",
                null,
                t_FBEventParameter
            );

        }

        public void FBALevelFailed(int t_CurrentLevel)
        {
            var t_FBEventParameter = new Dictionary<string,object>();
            t_FBEventParameter["Level_Info"] = "" + t_CurrentLevel;
            FB.LogAppEvent(
                "Level_Failed",
                null,
                t_FBEventParameter
            );

        }

        public void FBRewardedVideoAd(string t_AdType)
        {

            FB.LogAppEvent(
                "RewardedVideoAd_" + t_AdType,
                1,
                null
            );
        }

        #endregion
    }


