﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;
    
    [Header("Upper panel")] public Animator upperPanelAnimator;
    public TextMeshProUGUI currentLevelText;
    
    
    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;
    public ParticleSystem levelCompleteParticle2;

    public GameObject levelCompleteHolder;



    public GameObject tutObject;
    public GameObject stickTutObject;

    [Header("PowerUp")]
    public RectTransform barTrans;

    public Image fillImage;
    public bool isFilling;
    public GameObject powerHolder;
    public GameObject powerHolderCircle;

    [Header("Dirty Bar")]
    public GameObject dirtyBarHolder;
    public Image dirtyFillImage;

    [Header("Level Failed")]
    public GameObject levelFailedHolder;
    public Button tryAgainButton;

    [Header("Tutorial")]
    public GameObject tutHolder;
    public Animator tutAnimator;

    public Button startButton;


    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }

//        Debug.Log(barTrans.anchoredPosition);
    }

    private void Start()
    {
        currentLevelText.text = "Level " +LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        ButtonInteraction();
    }
    public void LoadLevelNumber()
    {
        currentLevelText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
    }
    

    private void ButtonInteraction()
    {
       tapButton.onClick.AddListener(delegate
       {
          // startPanelAnimator.SetTrigger(EXIT);
           //upperPanelAnimator.SetTrigger(ENTRY);
          // Gameplay.Instance.StartGame();
       });
    }
    public void StartGame()
    {
        startPanelAnimator.SetTrigger(EXIT);

    }
    
    public void ShowLevelComplete()
    {
        //levelCompleteAnimator.SetTrigger(ENTRY);

        levelCompleteParticle.Play();
        levelCompleteParticle2.Play();
        levelCompleteHolder.SetActive(true);

        collectButton.onClick.AddListener(delegate
        {
            Gameplay.Instance.GotoNextLevel();   
        });
    }
    public void ActivateTut()
    {
        tutObject.SetActive(true);
    }
    public void DeactivateTut()
    {
        tutObject.SetActive(false);
    }

    public void ActivateStickTut()
    {
        stickTutObject.SetActive(true);
    }
    public void DeactivateStickTut()
    {
        stickTutObject.SetActive(false);
    }

    public void PowerUpFiller(bool isCircle)
    {
        
        isFilling = true;

        if (isCircle)
        {
            powerHolderCircle.SetActive(true);
            StartCoroutine(SmoothFillerRoutine());
        }
        else
        {
            powerHolder.SetActive(true);
            StartCoroutine(BarMoveRightRoutine());
        }
    }
    IEnumerator SmoothFillerRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.40f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = fillImage.fillAmount;
        float t_DestValue = 1;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (isFilling)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            fillImage.fillAmount = t_Progression;

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(SmoothFillerRoutine());
    }

    public void PowerupReset()
    {
        Debug.LogError(barTrans.anchoredPosition.x);
        fillImage.fillAmount = 0;
        isFilling = false;
        powerHolder.SetActive(false);
        powerHolderCircle.SetActive(false);
        StopCoroutine(BarMoveRightRoutine());
        StopCoroutine(BarMoveLeftRoutine());
    }

    IEnumerator BarMoveRightRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.5f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = -360;
        float t_DestValue = 360;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (isFilling)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            barTrans.anchoredPosition3D = new Vector3 (Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression),barTrans.anchoredPosition.y);

            if (t_Progression >= 1f)
            {
                StartCoroutine(BarMoveLeftRoutine());
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(BarMoveRightRoutine());
    }

    IEnumerator BarMoveLeftRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.5f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = 360;
        float t_DestValue = -360;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (isFilling)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            barTrans.anchoredPosition3D = new Vector3(Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression), barTrans.anchoredPosition.y);

            if (t_Progression >= 1f)
            {
                StartCoroutine(BarMoveRightRoutine());
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(BarMoveLeftRoutine());
    }

    public void ActivateDirtyBar()
    {
        dirtyBarHolder.SetActive(true);
    }


    public void DirtyFill(float value)
    {
        StartCoroutine(DirtyBarFillRoutine(value));
    }

    IEnumerator DirtyBarFillRoutine(float value)
    {
        float t_Progression = 0f;
        float t_Duration = 0.3f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = dirtyFillImage.fillAmount;
        float t_DestValue = dirtyFillImage.fillAmount + value;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            dirtyFillImage.fillAmount = Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression);

            if (t_Progression >= 1f)
            {
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(DirtyBarFillRoutine(0));
    }

    public void ActivateLevelFailed()
    {
        levelFailedHolder.SetActive(true);

        tryAgainButton.onClick.AddListener(delegate
        {
            Gameplay.Instance.LoadAgain();
        });
    }

    public void ActivateTutorial()
    {
        tutHolder.SetActive(true);
        tutAnimator.SetTrigger(ENTRY);
        startButton.onClick.AddListener(delegate
        {
            startButton.gameObject.SetActive(false);
            tutAnimator.SetTrigger(EXIT);
            Gameplay.Instance.isStart = true;

           
            

            //AdjustSDKManager.Instance.TrackAdjustEvent("jd8jb6");
        });
    }
}
